import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {LoginResponseDto} from "./login-response.dto";
import {Injectable} from "@angular/core";
import {UserDto} from "./user-dto";

@Injectable()
export class AdminClient {
  private baseUrl = "http://localhost:8080/api/v1";

  constructor(
    private readonly httpClient: HttpClient,
  ) {
  }

  getUsers(): Observable<any> {
    return this.httpClient.get<UserDto>(`${this.baseUrl}/admin/users`, {
      headers: {
        "Content-Type": "application/json"
      }
    });
  }

  activate(user: UserDto): Observable<any> {
    return this.httpClient.post<UserDto>(`${this.baseUrl}/admin/users/activate`,
      {
        email: user.email
      },
      {
        headers: {
          "Content-Type": "application/json"
        }
      });
  }

  deactivate(user: UserDto): Observable<any> {
    return this.httpClient.post<UserDto>(`${this.baseUrl}/admin/users/deactivate`,
      {
        email: user.email
      },
      {
        headers: {
          "Content-Type": "application/json"
        }
      });
  }
}
