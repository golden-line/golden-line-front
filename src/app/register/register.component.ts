import { Component } from '@angular/core';
import {FormsModule} from "@angular/forms";
import {LoginClient} from "../client/login.client";
import {TokenService} from "../client/token-service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [
    FormsModule
  ],
  providers: [LoginClient, TokenService],
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {

    constructor(
      private loginClient: LoginClient,
      private router: Router,
    ) {}

    user = {
      email: '',
      password: '',
      fullName: ''
    };

    onSubmit() {
      this.loginClient.register(this.user.email, this.user.password, this.user.fullName).subscribe({
        next: () => {
          this.router.navigate(['/login']);
        },
        error: (error) => {
          console.error('Registration failed', error);
        }
      });
    }
}
