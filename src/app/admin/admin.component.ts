import {Component, OnInit} from '@angular/core';
import {DatePipe} from "@angular/common";
import {AdminClient} from "../client/admin.client";
import {UserDto} from "../client/user-dto";

@Component({
  selector: 'app-admin',
  standalone: true,
  providers: [AdminClient],
  imports: [
    DatePipe
  ],
  templateUrl: './admin.component.html',
  styleUrl: './admin.component.css'
})
export class AdminComponent implements OnInit {

  users: UserDto[] = [];

  ngOnInit() {
    this.adminClient.getUsers().subscribe(users => {
      this.users = users.sort((a: { fullName: string; }, b: { fullName: any; }) => a.fullName.localeCompare(b.fullName));
    });
  }

  constructor(private readonly adminClient: AdminClient) {}


  activate(user: UserDto) {
    this.adminClient.activate(user).subscribe({
      complete: () => {
        this.ngOnInit();
      },
      error: (error) => {
        alert(`error activating ${user.fullName}`);
      }
    })
  }

  deactivate(user: UserDto) {
    this.adminClient.deactivate(user).subscribe({
      complete: () => {
        this.ngOnInit();
      },
      error: (error) => {
        alert(`error deactivating ${user.fullName}`);
      }
    })
  }
}
