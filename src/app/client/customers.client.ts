import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {TokenService} from "./token-service";

@Injectable()
export class CustomersClient {
  private baseUrl = "http://localhost:8080/api/v1";

  constructor(
    private readonly httpClient: HttpClient,
    private readonly tokenService: TokenService,
  ) {
  }

  getCustomersStats(): Observable<any> {
    return this.httpClient.get(
      `${this.baseUrl}/customers/expenditure-by-soci-professional-category`,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.tokenService.getToken}`
        })
      }
    );
  }

  getCustomersStatsMean(): Observable<any> {
    return this.httpClient.get(
      `${this.baseUrl}/customers/mean-expenditure-by-soci-professional-category`,
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${this.tokenService.getToken}`
        })
      }
    );
  }


}
