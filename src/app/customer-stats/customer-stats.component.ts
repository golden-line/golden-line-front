import {Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import {AgChartsAngular} from 'ag-charts-angular';
import {AgChartOptions} from 'ag-charts-community';
import {CustomersClient} from "../client/customers.client";
import {isPlatformBrowser, NgIf} from "@angular/common";
import {MatCard, MatCardContent, MatCardHeader, MatCardSubtitle, MatCardTitle} from "@angular/material/card";
import {FlexLayoutModule} from "@angular/flex-layout";
import {TokenService} from "../client/token-service";
import {MatIcon} from "@angular/material/icon";

@Component({
  selector: 'app-customer-stats',
  standalone: true,
  imports: [AgChartsAngular, MatCard, MatCardHeader, MatCardContent, MatCardSubtitle, MatCardTitle, FlexLayoutModule, NgIf, MatIcon
  ],
  templateUrl: './customer-stats.component.html',
  styleUrl: './customer-stats.component.css',
  providers: [CustomersClient, TokenService]
})
export class CustomerStatsComponent {
  // Chart Options
  public statsChartOptions: AgChartOptions = {};
  public meanStatsChartOptions: AgChartOptions = {};
  public isBrowser: boolean = false;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private customersClient: CustomersClient
  ) {
    this.isBrowser = isPlatformBrowser(this.platformId);
    this.updateStats();
  }


  updateStats(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.customersClient.getCustomersStats().subscribe(customers => {
        console.log(customers);
        this.statsChartOptions = {
          // Data: Data to be displayed in the chart
          data: customers.map((customer: { first: string; second: string; }) => {
              return {
                category: customer.first,
                value: customer.second
              };
            }
          ),

          // Series: Defines which chart type and data to use
          series: [{type: 'bar', xKey: 'category', yKey: 'value'}]
        };
      });
      this.customersClient.getCustomersStatsMean().subscribe(customers => {
        console.log(customers);
        this.meanStatsChartOptions = {
          // Data: Data to be displayed in the chart
          data: customers.map((customer: { first: string; second: string; }) => {
              return {
                category: customer.first,
                value: customer.second
              };
            }
          ),

          // Series: Defines which chart type and data to use
          series: [{type: 'bar', xKey: 'category', yKey: 'value'}]
        };
      });

    }
  }

}
