import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {LoginResponseDto} from "./login-response.dto";
import {TokenService} from "./token-service";
import {Observable} from "rxjs";

@Injectable()
export class LoginClient {
  private baseUrl = "http://localhost:8080/api/v1";
  constructor(
    private readonly httpClient: HttpClient,
    private readonly tokenService: TokenService,
  ) {}

  login(email: string, password: string): Observable<any> {
    return this.httpClient.post<LoginResponseDto>(`${this.baseUrl}/auth/login`, {email, password}, {
      headers: {
        "Content-Type": "application/json"
      }
    });
  }

  register(email: string, password: string, fullName: string): Observable<any> {
    return this.httpClient.post(`${this.baseUrl}/auth/signup`, {email, password, fullName}, {
      headers: {
        "Content-Type": "application/json"
      }
    });
  }

}
