import {HttpErrorResponse, HttpEvent, HttpHandlerFn, HttpHeaders, HttpRequest} from '@angular/common/http';
import { inject } from '@angular/core';
import {catchError, Observable} from 'rxjs';
import {TokenService} from "./token-service";
import {Router} from "@angular/router";

export function authInterceptor(req: HttpRequest<unknown>, next: HttpHandlerFn): Observable<HttpEvent<unknown>> {
  const tokenService = inject(TokenService);
  const router = inject(Router);
  const token = tokenService.getToken;
  let headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });
  if(token) {
    headers = headers.set('Authorization', `Bearer ${token}`);
  }

  const newReq = req.clone({
    headers
  })

  return next(newReq).pipe(
    catchError((error: HttpErrorResponse) => {
      if(error.status === 403) {
        tokenService.resetToken();
        void router.navigate(['/login']);
      }
      console.error('Erreur de la requête:', error);
      throw error;
    })
  )
}
