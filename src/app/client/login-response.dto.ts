export interface LoginResponseDto {
  token: string,
  role: string,
  expireIn: number,
}
