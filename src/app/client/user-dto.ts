export interface UserDto {
  email: string,
  fullName: string,
  role: string,
  createdAt: string,
  active: boolean,
}
