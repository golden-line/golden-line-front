import { Component } from '@angular/core';
import {FormsModule} from "@angular/forms";
import {LoginClient} from "../client/login.client";
import {Router} from "@angular/router";
import {TokenService} from "../client/token-service";
import {LoginResponseDto} from "../client/login-response.dto";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    FormsModule,
    NgIf,
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
  providers: [LoginClient, TokenService]
})
export class LoginComponent {

  error = false;
  errorMessage = '';

  constructor(
    private readonly loginClient: LoginClient,
    private readonly router: Router,
    private readonly tokenService: TokenService,

) {}

  user = {
    email: '',
    password: ''
  };

  onSubmit() {
    this.loginClient.login(this.user.email, this.user.password).subscribe(
      {
        next: (response: LoginResponseDto) => {
          this.tokenService.setToken = response.token;
          this.tokenService.setRole = response.role;
        },
        complete: () => {
          void this.router.navigate(['/stats']);
        },
        error: (error) => {
          this.error = true;
          this.errorMessage = `Error login (invalid credentials or account not activated)`;
        }
      }
    );
  }
}
