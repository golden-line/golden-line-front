import { Component } from '@angular/core';
import {RouterLink} from "@angular/router";
import {NgIf, NgOptimizedImage} from "@angular/common";
import {MatToolbar} from "@angular/material/toolbar";
import {TokenService} from "../client/token-service";
import {MatCard, MatCardHeader} from "@angular/material/card";
import {AgChartsAngular} from "ag-charts-angular";
import {MatIcon} from "@angular/material/icon";

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [
    RouterLink,
    NgOptimizedImage,
    MatToolbar,
    MatCard,
    MatCardHeader,
    AgChartsAngular,
    NgIf,
    MatIcon
  ],
  providers: [TokenService],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.css'
})
export class NavbarComponent {
  constructor(private readonly tokenService: TokenService) {
  }

  logout() {
    this.tokenService.setToken = '';
  }

  isLoggedIn(): boolean {
    return this.tokenService.getToken !== '';
  }

  isAdmin(): boolean {
    return this.tokenService.getRole === 'ROLE_ADMIN';
  }

}
