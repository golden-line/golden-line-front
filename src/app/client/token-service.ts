import {Injectable} from "@angular/core";

@Injectable()
export class TokenService {

    get getToken(): string | null {
        return localStorage.getItem('token');
    }

    set setToken(token: string) {
        localStorage.setItem('token', token);
    }

    resetToken() {
      localStorage.removeItem('token');
    }

    get getRole(): string | null {
      return localStorage.getItem('role');
    }

    set setRole(role: string) {
      localStorage.setItem('role', role);
    }
}
